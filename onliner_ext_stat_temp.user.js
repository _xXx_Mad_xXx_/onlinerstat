// ==UserScript==
// @author      seriych
// @creator     seriych
// @contributor kostya500
// @name        onlinerExtendedStat
// @version     2016.12.20.0
// @description Adds some usable fields for WoT topic on forum.onliner.by
// @namespace   https://profile.onliner.by/user/1699665
// @icon        http://www.modxvm.com/assets/xvm.png
// @match       https://forum.onliner.by/viewtopic.php?t=*
// @include     https://forum.onliner.by/viewtopic.php?t=*
// @updateURL   https://bitbucket.org/seriych/onlinerextendedstat/raw/tip/onliner_ext_stat.user.js
// @downloadURL https://bitbucket.org/seriych/onlinerextendedstat/raw/tip/onliner_ext_stat.user.js
// @grant       GM_xmlhttpRequest
// @connect     api.worldoftanks.ru
// ==/UserScript==

// mailto: i-_-l[at]mail.ru

// Цвета статистики
var statcolors = {
    undefined:    "#000000",   // неопределенно   undefined
    very_bad:     "#FE0E00",   // очень плохо     very bad
    bad:          "#FE7903",   // плохо           bad
    normal:       "#BBAA00",   // средне          normal
    good:         "#00AA00",   // хорошо          good
    very_good:    "#00AACC",   // очень хорошо    very good
    unique:       "#D042F3"    // уникально       unique
};

var hour = 1000*60*60;
var expirehours = 6; // срок годности данных в кэше (в единицах переменной hour- по умолчанию в часах)

var clanicon = {
    type:   "portal",
    size:   24          // 24 32
};

// топики, в которых работать скрипту
var includes = [
    'forum.onliner.by/viewtopic.php?t=16624179'
];

// ключ приложения для API WG
var api_key = '110ca0fd8b6400ea5257c90d551374ae';


// скрипт запроса в API
/* Requires _opera-xdr-engine.js to handle script-based requests in Opera*/
var xdr = {
    reqId: 0,
        req: {},
        prepareUrl: function (url) {
            return url;
        },
        xget: function (url, onDone) {
            url = this.prepareUrl(url);
            if (window.opera && window.opera.defineMagicVariable)
                this.scriptTransport(url, onDone);
            else
                this.GMTransport(url, onDone);
        },
        scriptTransport: function (url, onDone) {
            var t = document.createElement("script");
            t.src = url;
            t._callback = onDone;
            document.body.appendChild(t);
        },
        xhrTransport: function (url, onDone) {
            var req = new XMLHttpRequest();
            req.open("GET", url, true);
            req.onreadystatechange = function () {
                if (req.readyState == 4)
                    if (req.status == 200)
                        onDone(req.responseText);
            };
            req.send();
        },
        GMTransport: function (url, onDone) {
            setTimeout(function () {
                GM_xmlhttpRequest({
                    method: "GET",
                    url: url,
                    onload: function (x) {
                        var o = x.responseText;
                        if (onDone)
                            onDone(o);
                    }
                });
            }, 0);

        },
        JSONPTransport: function (url, callbackName) {
            if (callbackName && typeof callbackName === "string")
                url += "&callback=" + callbackName;
            var t = document.createElement("script");
            t.src = url;
            document.body.appendChild(t);
        }
    };

// Локализация / Localization
var l10n = {
    ban_last :  "Последний бой: ",
    ban_diff :  "Разность, дней: ",
    ban_left :  "Осталось, дней: "
};

// Запрос онлайна, если мы в нужной ветке форума
if ( ~String(window.location).indexOf('forum.onliner.by/viewtopic.php') )
    xdr.xget("https://api.worldoftanks.ru/wgn/servers/info/?application_id=" + api_key + "&game=wot", outWGonline);

// Выполняем обработку, если мы на форуме
if ( ~String(window.location).indexOf('forum.onliner.by/viewtopic.php') ) {
    // смотрим количество сообщений и работаем, только если их больше 1
    var Apostclass = document.getElementsByClassName("mtauthor-nickname");
    var Nposts = Apostclass.length;
    if ( Nposts>1 ) {
        // база игроков на текущей странице
        var base = {
            onicks: [],
            wgnicks: [],
            ids: [],
            clanids: [],
            nums: [],
            stats: []
        };
        // база всех известных игроков
        var obase = {
            onicks: [],
            ids: [],
            clanids: [],
            table: [],
            list: {}
        };

        // Вручную добавленные юзеры
        var onliner_users = [];
        // достаем данные о юзерах из кэша
        if (localStorage["onlinerUsers"] != undefined) {
            var lsUsers = JSON.parse(localStorage.getItem('onlinerUsers'));
            onliner_users = lsUsers.Ausers;
        }
        // если кэш пустой или просроченный, запрашиваем файл юзеров с репозитория
        if (typeof lsUsers == "undefined" || expired(lsUsers.time)) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", 'https://bitbucket.org/_xXx_Mad_xXx_/onlinerstat/raw/master/onlinerUsers.json', true);
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4) {
                    if (xmlhttp.status == 200) {
                        var onliner_resp = JSON.parse(xmlhttp.responseText);
                        var lsUsers = {
                            'Ausers' : [],
                            'time'  : (Date.now()/hour).toFixed(0)
                        };
                        for (var id in onliner_resp)
                            lsUsers.Ausers.push([onliner_resp[id], id]);
                        // перезаписываем кэш
                        localStorage.setItem('onlinerUsers', JSON.stringify(lsUsers));
                    }
                }
            };
            xmlhttp.send(null);
        }
        // добавляем к списку юзеров
        for ( var i=0; i<onliner_users.length; i++ )
            if ( !~indexStr(onliner_users[i][0], obase.onicks) ) {
                var obaselen = obase.onicks.length;
                obase.onicks.push(onliner_users[i][0]);
                obase.ids.push(onliner_users[i][1]);
            }

        // ищем авторов всех записей на странице
        for ( var i=0; i<Apostclass.length; i++ )
            if ( Apostclass[i].getElementsByClassName("_name star-notes").length>0 ) {
                var tmponick = Apostclass[i].getElementsByClassName("_name star-notes")[0].innerHTML;
                var obaseindex = indexStr(tmponick, obase.onicks);
                var baseindex  = indexStr(tmponick,  base.onicks);
                if ( ~obaseindex )
                    if ( ~baseindex )
                        base.nums[baseindex].push(i);
                    else {
                        base.onicks.push(tmponick);
                        base.ids.push(obase.ids[obaseindex]);
                        base.nums.push([i]);
                    }
        }

        // запрос информации о юзерах в API
        xdr.xget("https://api.worldoftanks.ru/wot/account/info/?application_id=" + api_key + "&fields=nickname,global_rating,clan_id,statistics.all.battles,statistics.all.wins&account_id="+base.ids.join(','), outWGStatOC);

        // ищем список юзеров в шапке темы
        var usersSpoiler = document.getElementsByClassName('msgpost-spoiler-hd');
        var spoilerNum = 0;
        while ( spoilerNum<usersSpoiler.length && usersSpoiler[spoilerNum].innerHTML.toUpperCase()!="СТАТИСТИКА ИГРОКОВ КЛУБА" )
            spoilerNum++;
        if ( spoilerNum < usersSpoiler.length ) {
            // добавляем таблицу игроков
            var CuserList = usersSpoiler[spoilerNum].parentNode.parentNode.getElementsByClassName("msgpost-spoiler-txt")[0];
            var CuserTable = document.createElement('table');
            CuserTable.className = 'userListWotStat';
            CuserList.appendChild(CuserTable);
            var style = document.createElement('style');
            style.innerHTML  = '.userListWotStat {border: none; border-spacing: 0px; width: 920px;} \
                                .userListWotStat td, .userListWotStat th {height: 21px; background: transparent; padding: 0px; margin: 0px; color: #000; text-align: right;} \
                                .userListWotStat th {font-weight: normal; font-family: Consolas, Arial, monospace; font-size: 14px;} \
                                .userListWotStat th:hover {color: #00B !important; font-weight: bold !important;} \
                                .userListClan {text-align: left !important; font-family: Consolas, monospace; font-size: 14px;} \
                                .userListWGnick {padding-left: 15px !important;} \
                                .userListWotStat div {padding-top: 2px; float: right; right: 0px; width: 20px; font-size: smaller;} \
                                .userListWotStat tbody tr > td > a > img {margin: -1px; height: 20px; width: 20px; position:relative; z-index: 1;} \
                                .userListWotStat tbody tr:hover > td > a > img {margin: -11px; height: 32px; width: 32px; position:relative; z-index: 10; background: #F9FAFA; border: 4px solid #F9FAFA;} \
                                .userListWotStat tbody tr:hover {background: white;}';
            CuserTable.appendChild(style);
            // ширина столбцов
            var colgroup = document.createElement('colgroup');
            colgroup.innerHTML = '<col width="125px"/><col width="40px"/><col width="55px"/><col width="62px"/><col width="62px"/><col width="66px"/><col width="155px"/><col width="70px"/><col width="40px"/><col width="40px"/><col width="120px"/><col width="85px"/>';
            CuserTable.appendChild(colgroup);

            // заголовок таблицы игроков
            var thead = document.createElement('tr');
            thead.style.cursor = 'pointer';
            thead.innerHTML = '<th id="WoTtableCol00" style="text-align: left;">Ник<b></b></th> \
                               <th id="WoTtableCol01"><b></b>Бои</th> \
                               <th id="WoTtableCol02"><b></b>WR</th> \
                               <th id="WoTtableCol03"><b></b>WN8</th> \
                               <th id="WoTtableCol04"><b></b>EFF</th> \
                               <th id="WoTtableCol05"><b></b>WGR</th> \
                               <th id="WoTtableCol06" style="text-align: left;"><font class="userListWGnick">Ник в игре</font><b></b></th> \
                               <th id="WoTtableCol07" style="text-align: left;">Клан<b></b></th> \
                               <th id="WoTtableCol08" title="Место клана в рейтинге на Глобальной Карте"><b></b>wGM</th> \
                               <th id="WoTtableCol09" title="Место клана в рейтинге в Укрепрайонах"><b></b>wSH</th> \
                               <th id="WoTtableCol10"><b></b>' + l10n.ban_last.replace(/:.*/g, '') + '</th>';
            CuserTable.appendChild(thead);
            // параметры сортировки и определение вызова функции сортировки
            var sortColList = {
                titles     : ['onick', 'kb', 'wr', 'wn8', 'eff', 'wgr', 'wgnick', 'clan_tag', 'clan_egm', 'clan_esh', 'last_time'],
                directions : [1, -1, -1, -1, -1, -1, 1, -1, 1, 1, -1, -1]
            };
            var sortCol = 0;
            var Ath = CuserTable.lastChild.getElementsByTagName("th");
            for ( var i=0; i<Ath.length; i++ )
                Ath[i].onclick = function() {
                    writeUsersTable(this.id.slice(-2));
                };
            var Ab = CuserTable.lastChild.getElementsByTagName("b");
            // тело таблицы игроков
            CuserTable.appendChild(document.createElement('tbody'));
            var CuserTable = CuserTable.lastChild;
            var Ngets = 0;
            var Nresps = 0;
            var Nclangets = 0;
            var Nclanresps = 0;

            // достаем данные о юзерах из кэша
            var bclans_ready = false;
            var bratings_ready = false;
            if (localStorage["tableUsers"] != undefined) {
                var lsTableUsers = JSON.parse(localStorage.getItem('tableUsers'));
                obase.table = lsTableUsers.table;
            }
        }
    }
}

// обработка информации из API о юзерах
function outWGStatOC(response) {
    var onliner_resp = JSON.parse(response);
    // вставляем стату для всех сообщений, соответствующих данному id
    for (var id in onliner_resp.data) {
        // обрабатываем стату
        var onliner_battles = onliner_resp.data[id].statistics.all.battles;
        var onliner_wins    = onliner_resp.data[id].statistics.all.wins;
        var nickname        = onliner_resp.data[id].nickname;
        var clan_id         = onliner_resp.data[id].clan_id;
        var wgr             = onliner_resp.data[id].global_rating;
        var winrate         = 100.0*onliner_wins/onliner_battles;
        if ( onliner_battles<9950 )
            var onliner_kb = ((0.0+onliner_battles)/1000).toFixed(1);
        else
            var onliner_kb = ((0.0+onliner_battles)/1000).toFixed(0);
        // формируем строку статы
        var onliner_ratings = "<td><a href='http://www.noobmeter.com/player/ru/" + nickname + "' target='_blank' title='кило-бои\nссылка: noobmeter'><b>" + onliner_kb + 'k</b></a></td>' + "<td> </td><td><a href='http://wot-news.com/stat/calc/ru/ru/" + nickname + "' target='_blank' title='Процент побед\nссылка: wot-news'>" + '<font color="' + CalcWrColor(winrate) + '"><b>' + winrate.toFixed(1) + '%</b></font></a></td><td> </td>' + "<td><a href='http://wots.com.ua/user/stats/" + nickname + "' target='_blank' title='Личный Рейтинг Игрока WG\nссылка: wots.com.ua'>" + '<font color="' + CalcXVMColor(CalcXrate.wgr(wgr)) + '"><b>' + wgr + '</b></font></a></td>';
        var indexbase = base.ids.indexOf(id);
        if ( ~indexbase )
            base.wgnicks[indexbase] = nickname;
        // запоминаем id клана, если есть
        if ( ~indexbase && clan_id!=null )
            base.clanids[indexbase] = clan_id;
        // обрезаем слишком длинные ники
        if (clan_id!=null && nickname.length > 16)
            nickname = nickname.substr(0, 15) + "..";
        else if (nickname.length > 22)
            nickname = nickname.substr(0, 21) + "..";
        // вставляем стату во все сообщения данного автора
        for (var j=0; j<base.nums[indexbase].length; j++) {
            var i = base.nums[indexbase][j];
            var f = document.createElement('font');
            f.size = '-2';
            f.innerHTML = "<table style='white-space: pre-wrap;'><tr>" + onliner_ratings + "</tr></table>";
            Apostclass[i].appendChild(f);
            if ( isInclude(String(window.location)) && Apostclass[i].getElementsByClassName("_name star-notes").length>0 )
                Apostclass[i].getElementsByClassName("_name star-notes")[0].title += ' (wot: ' + nickname + ' ' + onliner_kb + 'k ' + winrate.toFixed(1) + '% ' + wgr + ')';
            // сразу рисуем никнейм, если игрок не в клане
            if ( clan_id==null && id!='8938868') {
                var a = document.createElement('a');
                a.href = 'http://worldoftanks.ru/community/accounts/' + id + '-';
                a.target = '_blank';
                a.title = 'Никнейм в танках\nссылка: wargaming';
                a.innerHTML = "<font size='-2' color='#000'><b>" + nickname + "</b></font>";
                Apostclass[i].appendChild(a);
            } else if (id == '8938868') {    // Персональная иконка какому-то оленю
                var t = document.createElement('table');
                t.innerHTML = "<tr><td><a href='http://www.modxvm.com' target='_blank' title='Официальный сайт мода XVM\nссылка: modxvm.com'>" + '<img src="http://www.modxvm.com/assets/xvm.png" alt="XVM" width="24" height="24" align="left" /></a></td>' + "<td><a href='http://worldoftanks.ru/community/accounts/" + id + "-' target='_blank' title='wargaming'><font size='-2' color='#000'><b>" + nickname + "</b></font></a></td></tr>";
                Apostclass[i].appendChild(t);
            }
        }
    }

    // запрос информации о кланах в API
    // (удалять повторяющиеся не обязательно, API отдает только уникальные)
    var clans_string = base.clanids.join(',').replace(/,,+/g, ',');
    if ( clans_string.length>0 )
        xdr.xget("https://api.worldoftanks.ru/wgn/clans/info/?application_id=" + api_key + "&fields=tag,name,motto,members_count,clan_id,emblems.x"+clanicon.size+"&clan_id=" + clans_string, outWGclanStatOC);
}

// обработка информации из API о кланах
function outWGclanStatOC(response) {
    clan_resp = JSON.parse(response);
    for (var id in clan_resp.data)
        if ( id!=null && id !=undefined ) {
            // обрабатываем стату
            var abbr    = clan_resp.data[id].tag;
            var name    = clan_resp.data[id].name;
            var motto   = clan_resp.data[id].motto;
            var members = clan_resp.data[id].members_count;
            var clan_id = clan_resp.data[id].clan_id;
            var icourl  = clan_resp.data[id].emblems['x'+clanicon.size][clanicon.type];
            // вставляем данные клана во все посты от членов этого клана
            var indexbase = base.clanids.indexOf(clan_id);
            while ( ~indexbase ) {
                for (var j=0; j<base.nums[indexbase].length; j++) {
                    var i = base.nums[indexbase][j];
                    var t = document.createElement('table');
                    t.innerHTML = "<tr><td rowspan='2'><a href='http://ru.wargaming.net/clans/wot/" + clan_id + "' target='_blank' title='" + clanTitle(abbr, members, name, motto, true) + "'>" + '<img src="'+ icourl + '" alt="' + abbr + '" width="' + clanicon.size + '" height="' + clanicon.size + '" align="left" /></a></td>' + "<td>" + "<a href='http://worldoftanks.ru/community/accounts/" + base.ids[indexbase] + "-' target='_blank' title='Никнейм в танках\nссылка: wargaming'><font size='-2' color='#000'><b>" + base.wgnicks[indexbase] + "</b></font></a></td></tr>" + "<tr><td><a href='http://ivanerr.ru/lt/clan/" + clan_id + "' target='_blank' title='" + clanTitle(abbr, members, name, motto, false) + "'><font color='#000' size='-2'>[ " + abbr + " ]</font></a></td></tr>";
                    Apostclass[i].appendChild(t);
                    if ( Apostclass[i].getElementsByClassName("_name star-notes").length>0 )
                        Apostclass[i].getElementsByClassName("_name star-notes")[0].title = Apostclass[i].getElementsByClassName("_name star-notes")[0].title.replace(/(wot:\s+\S+\s)/, '$1[' + abbr + '] ');
                }
                base.clanids[indexbase] += "_";
                indexbase = base.clanids.indexOf(clan_id);
            }
        }

    // запрашиваем стату для таблицы игроков в шапке темы
    // если кэш пустой или просроченный, запрашиваем c API
    if (typeof lsTableUsers == "undefined" || expired(lsTableUsers.time))
        getBasicStat(false);
    else
        writeUsersTable(sortCol);
}

// формируем строку всплывающей подсказки о клане
function clanTitle(tag, members, name, motto, logo) {
    var tmpTitle = '&nbsp;[' + tag + ']\nИгроков: ' + members + '\n' + name + '\n' + motto + '\nссылка:&nbsp;';
    if (logo)
        return 'Логотип&nbsp;клана' + tmpTitle + 'wargaming';
    else
        return 'Клантег' + tmpTitle + 'ivanerr';
}

// обработка информации из API об онлайне
function outWGonline(response) {
    var resp = JSON.parse(response);
    var Aserv = [];
    var Aservstr = [];
    if (resp.data.wot.length > 0) {
        var sum = 0;
        for (var i=0; i<resp.data.wot.length; i++) {
            Aserv.push([resp.data.wot[i].server, resp.data.wot[i].players_online]);
            sum += Aserv[i][1];
        }
        Aserv.sort(compServs);
        for (var i=0; i<10; i++)
            if (Aserv[i][0] != 'RU' + (i+1))
                Aserv.splice(i, 0, ['RU' + (i+1), 0]);
        Aserv.push(['Всего', sum]);
        for (var i=0; i<Aserv.length; i++) {
            if ( Aserv[i][1] < 9950 )
                Aserv[i][1] = ((0.0+Aserv[i][1])/1000).toFixed(1);
            else
                Aserv[i][1] = ((0.0+Aserv[i][1])/1000).toFixed(0);

            Aservstr[i] = Aserv[i][0] + ': ' + Aserv[i][1] + 'k';
            if ( i==Aserv.length-1 )
                Aservstr[i] = '<font color="#000">' + Aservstr[i] + '</font>';
            else if ( Aserv[i][1] == 0 )
                Aservstr[i] = '<font color="#FF0000">' + Aserv[i][0] + ': --' + '</font>';
            else if ( Aserv[i][1] < 10 || Aserv[i][1] > 100 )
                Aservstr[i] = '<font color="#FE7903">' + Aservstr[i] + '</font>';
        }
    } else
        Aservstr = ['<font color="#FF0000">WoT servers OFFLINE</font>'];

    document.getElementsByClassName('b-txtsbn')[0].innerHTML = "<p align='center' style='font-family:arial,verdana; font-size:10pt; color:#666; font-weight:bold;'><font color='#000'>Онлайн:</font>&nbsp;&nbsp;&nbsp;&nbsp;" + Aservstr.join('&nbsp;&nbsp;&nbsp;&nbsp;') + '</p>';
}

// сравнение названий серверов (для сортировки)
function compServs(a, b) {
    var dl = String(a[0]).length - String(b[0]).length;
    if (dl == 0) {
        if (String(a[0]) > String(b[0]))
            return 1;
        else
            return -1;
    }
    else
        return dl;
}

// запрашиваем общую стату всех юзеров для таблицы юзеров в шапке
function getBasicStat(getwotscom) {
    Nresps = 0;
    var allids = [];
    for ( var i=0; i<obase.ids.length; i++ )
        allids.push(obase.ids[i]);
    var Atmpids = [];
    // разбиваем запросы на сегменты по N юзеров в каждом (ограничение api N<=100)
    var apiLimit = 100;
    Ngets = Math.ceil(allids.length/apiLimit);
    while (allids.length > 0) {
        if (Atmpids.length < apiLimit)
            Atmpids.push(allids.pop());
        if (Atmpids.length == apiLimit || allids.length == 0) {
            if (getwotscom)
                outBasicStatWots(Atmpids.join(','));
            else
                xdr.xget("https://api.worldoftanks.ru/wot/account/info/?application_id=" + api_key + "&fields=nickname,last_battle_time,clan_id,global_rating,statistics.all.battles,statistics.all.wins&account_id="+Atmpids.join(','), outBasicStat);
            Atmpids = [];
        }
    }
}

// обработка информации из WG API для таблицы юзеров в шапке
function outBasicStat(response) {
    var basic_resp = JSON.parse(response);
    Nresps++;
    // пишем стату в таблицу
    for (var id in basic_resp.data)
        if (basic_resp.data[id] != null) {
            var nickname = basic_resp.data[id].nickname;
            var wgr = basic_resp.data[id].global_rating;
            var battles = basic_resp.data[id].statistics.all.battles;
            var winrate = 100.0*basic_resp.data[id].statistics.all.wins/battles;
            if ( battles<9950 )
                var over_kb = ((0.0+battles)/1000).toFixed(1);
            else
                var over_kb = ((0.0+battles)/1000).toFixed(0);
            // добавляем информацию о бане и последнем бое
            var last_time = basic_resp.data[id].last_battle_time;
            var lt = new Date(last_time*1000);
            var last_string  = (lt.getYear() + 1900) + '.' + ('0' + (lt.getMonth() + 1)).slice(-2) + '.' + ('0' + lt.getDate()).slice(-2);
            var clan_id = basic_resp.data[id].clan_id;
            if (clan_id == null || clan_id == undefined)
                clan_id = '';
            else if (!~obase.clanids.indexOf(clan_id))
                obase.clanids.push(basic_resp.data[id].clan_id);
            obase.list[id] = {
                'id'        : id,
                'clan_id'   : basic_resp.data[id].clan_id,
                'clan_tag'  : '',
                'clan_ico'  : '',
                'clan_egm'  : '',
                'clan_esh'  : '',
                'onick'     : obase.onicks[obase.ids.indexOf(id)],
                'wgnick'    : basic_resp.data[id].nickname,
                'last_time' : last_string,
                'kb'        : over_kb,
                'wr'        : winrate,
                'wgr'       : wgr
            };
    }

    // запрашиваем рейтинги всех юзеров и информацию о их кланах
    if (Nresps == Ngets) {
        getBasicStat(true);
        getBasicClanStat(false);
    }
}

// запрашиваем общую стату всех юзеров для таблицы юзеров в шапке
function getBasicClanStat(getratings) {
    Nclanresps = 0;
    var allids = [];
    for ( var i=0; i<obase.clanids.length; i++ )
        allids.push(obase.clanids[i]);
    var Atmpids = [];
    // разбиваем запросы на сегменты по N юзеров в каждом (ограничение api N<=100)
    var apiLimit = 100;
    Nclangets = Math.ceil(allids.length/apiLimit);
    while (allids.length > 0) {
        if (Atmpids.length < apiLimit)
            Atmpids.push(allids.pop());
        if (Atmpids.length == apiLimit || allids.length == 0) {
            if (getratings)
                xdr.xget("https://api.worldoftanks.ru/wot/clanratings/clans/?application_id=" + api_key + "&fields=gm_elo_rating.rank,fb_elo_rating.rank&clan_id=" + Atmpids.join(','), outBasicTopClanStat);
            else
                xdr.xget("https://api.worldoftanks.ru/wgn/clans/info/?application_id=" + api_key + "&fields=emblems.x32,tag,name,motto,members_count,clan_id&clan_id=" + Atmpids.join(','), outBasicClanStat);
            Atmpids = [];
        }
    }
}

// обработка информации из wots.com.ua API для таблицы юзеров в шапке
function outBasicStatWots(ids) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('GET', 'https://wots.com.ua/api/accounts/ratings/register/' + ids, true);
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4) {
            Nresps++;
            if (xmlhttp.status == 200) {
                var wots_resp = JSON.parse(xmlhttp.responseText);
                for (var id in wots_resp.data)
                    if (wots_resp.data[id] != null)
                        if (obase.list[id] !== undefined) {
                            obase.list[id].wn8 = Number(wots_resp.data[id].rating_wn8) .toFixed(0);
                            obase.list[id].eff = Number(wots_resp.data[id].rating_eff) .toFixed(0);
                        }
            }
            // докладываем о готовности, когда все ответы получены
            if (Nresps == Ngets) {
                bratings_ready = true;
                basicStatReady();
            }
        }
    };
    xmlhttp.send(null);
}

// обработка информации из API о кланах для таблицы юзеров в шапке
function outBasicClanStat(response) {
    var bclan_resp = JSON.parse(response);
    Nclanresps++;
    for (var clan_id in bclan_resp.data)
        if ( clan_id != null && clan_id != undefined )
            for (var id in obase.list)
                if (obase.list[id].clan_id == clan_id) {
                    obase.list[id].clan_tag     = bclan_resp.data[clan_id].tag;
                    obase.list[id].clan_name    = bclan_resp.data[clan_id].name;
                    obase.list[id].clan_motto   = bclan_resp.data[clan_id].motto;
                    obase.list[id].clan_members = bclan_resp.data[clan_id].members_count;
                    obase.list[id].clan_ico     = bclan_resp.data[clan_id].emblems.x32.portal;
                }
    // получаем рейтинги кланов
    if (Nclanresps == Nclangets)
        getBasicClanStat(true);
}

// Получаем рейтинги кланов для таблицы юзеров в шапке
function outBasicTopClanStat(response) {
    var btop_clans = JSON.parse(response);
    Nclanresps++;
    for (var clan_id in btop_clans.data)
        if (clan_id != null && clan_id != undefined && btop_clans.data[clan_id] != undefined) {
            var egm = btop_clans.data[clan_id].gm_elo_rating.rank;
            var esh = btop_clans.data[clan_id].fb_elo_rating.rank;
            var segm = '';
            var sesh = '';
            if (egm != null && egm < 1000)
                segm = egm;
            if (esh != null && esh < 1000)
                sesh = esh;
            for (var id in obase.list)
                if (obase.list[id].clan_id == clan_id) {
                    obase.list[id].clan_egm = segm;
                    obase.list[id].clan_esh = sesh;
                }
        }
    // докладываем о готовности
    if (Nclanresps == Nclangets) {
        bclans_ready = true;
        basicStatReady();
    }
}

// обрабатываем полностью полученную статистику для общей таблицы игроков
function basicStatReady() {
    if (bclans_ready && bratings_ready) {
        obase.table = [];
        for (var key in obase.list)
            obase.table.push(obase.list[key]);
        // сохраняем таблицу в кэш
        var lsTableUsers = {
            'table' : obase.table,
            'time'  : (Date.now()/hour).toFixed(0)
        };
        localStorage.setItem('tableUsers', JSON.stringify(lsTableUsers));
        // добавляем таблицу юзеров на страничку
        writeUsersTable(sortCol);
    }
}

// добавляем таблицу юзеров на страничку
function writeUsersTable(Ns) {
    // добавляем таблицу юзеров на страничку, когда все ответы получены
    sortCol = Number(Ns);
    CuserTable.innerHTML = '';
    obase.table.sort(sortTable);
    addSortArrow(sortCol);
    sortColList.directions[sortCol] = -sortColList.directions[sortCol];
    for (var i=0; i<obase.table.length; i++) {
        var user = obase.table[i];
        var nickname = user.wgnick;
        // обрезаем слишком длинные ники
        var snickname = nickname;
        if (nickname.length > 15)
            snickname = nickname.substr(0, 14) + "..";
        var id = user.id;
        var sclan = '';
        //alert(bt);
        //Date.now()
        if (user.clan_ico != '')
            sclan = "<a href='http://ru.wargaming.net/clans/wot/" + user.clan_id + "' target='_blank' title='" + clanTitle(user.clan_tag, user.clan_members, user.clan_name, user.clan_motto, true) + "'>" +
                        '<img align="right" src="'+ user.clan_ico + '" alt="' + user.clan_tag + '"/></a>' +
                        "<a class='userListClan' href='http://ivanerr.ru/lt/clan/" + user.clan_id + "' target='_blank' title='" + clanTitle(user.clan_tag, user.clan_members, user.clan_name, user.clan_motto, false) + "'>" + user.clan_tag + "</a>";
        var sttmp = '<td style="text-align: left;">' + user.onick + '</td>' +
                    "<td><a href='http://www.noobmeter.com/player/ru/" + nickname + "' target='_blank' title='кило-бои\nссылка: noobmeter'><font color='black'>" + user.kb + 'k</font></a></td>' +
                    "<td><a href='http://wot-news.com/stat/calc/ru/ru/" + nickname + "' target='_blank' title='Процент побед\nссылка: wot-news'>" + '<font color="' + CalcWrColor(user.wr) + '">' + user.wr.toFixed(1) + '%</font></a></td>' +
                    "<td><a href='http://wots.com.ua/user/stats/" + nickname + "' target='_blank' title='Рейтинг WN8\nссылка: wots.com.ua'>" + '<font color="' + CalcXVMColor(CalcXrate.wn8(user.wn8)) + '">' + user.wn8 + '<div>' + ('0' + CalcXrate.wn8(user.wn8)).slice(-2) + '</div></font></a></td>' +
                    "<td><a href='http://wots.com.ua/user/stats/" + nickname + "' target='_blank' title='Рейтинг EFF\nссылка: wots.com.ua'>" + '<font color="' + CalcXVMColor(CalcXrate.eff(user.eff)) + '">' + user.eff + '<div>' + ('0' + CalcXrate.eff(user.eff)).slice(-2) + '</div></font></a></td>' +
                    "<td><a href='http://wots.com.ua/user/stats/" + nickname + "' target='_blank' title='Рейтинг WGR\nссылка: wots.com.ua'>" + '<font color="' + CalcXVMColor(CalcXrate.wgr(user.wgr)) + '">' + user.wgr + '<div>' + ('0' + CalcXrate.wgr(user.wgr)).slice(-2) + '</div></font></a></td>' +
                    "<td style='text-align: left;'><font class='userListWGnick'><a href='http://worldoftanks.ru/community/accounts/" + id + "-' target='_blank' title='Никнейм в танках: " + nickname + "\nссылка: wargaming'>" + snickname + "</a></font></td>" +
                    "<td style='text-align: left;'>" + sclan + "</td>" +
                    "<td><a href='http://ru.wargaming.net/clans/leaderboards/#ratingssearch&offset=0&limit=100&order=-egm&timeframe=all&clan_id=" + user.clan_id + "' target='_blank' title='Место клана в рейтинге на Глобальной Карте\nссылка: wargaming'>" + user.clan_egm + '</a></td>' +
                    "<td><a href='http://ru.wargaming.net/clans/leaderboards/#ratingssearch&offset=0&limit=100&order=-esh&timeframe=all&clan_id=" + user.clan_id + "' target='_blank' title='Место клана в рейтинге в Укрепрайонах\nссылка: wargaming'>" + user.clan_esh + '</a></td>' +
                    "<td><font color='black' title='" + l10n.ban_last + user.last_time + "'>" + user.last_time + '</font></td>';
        var Csubtitle = document.createElement('tr');
        Csubtitle.innerHTML = sttmp;
        CuserTable.insertBefore(Csubtitle, CuserTable.firstChild);
    }
}

// добавляем стрелку направления сорттировки в таблицу игроков
function addSortArrow(sortCol) {
    for ( var i=0; i<Ab.length; i++ ) {
        Ab[i].innerHTML = '';
        Ab[i].parentNode.style.fontWeight = 'normal';
        Ab[i].parentNode.style.color = '#000';
    }
    Ab[sortCol].parentNode.style.fontWeight = 'bold';
    Ab[sortCol].parentNode.style.color = '#00B';
    if (sortColList.directions[sortCol] < 0)
        Ab[sortCol].innerHTML = '↓'; // &darr;  &#8595;
    else
        Ab[sortCol].innerHTML = '↑'; // &uarr;  &#8593;
}

// ищем строку в массиве без учета регистра
function indexStr(s, A) {
    s = s.toUpperCase();
    var i = 0;
    while (i < A.length) {
        if (s == A[i].toUpperCase())
            return i;
        i++;
    }
    return -1;
}

// функция сравнения для сортировки в таблице
function sortTable(a, b) {
    if (sortCol >=1 && sortCol <= 5 || sortCol >=8 && sortCol <= 9) {
        var atmp = Number(a[sortColList.titles[sortCol]]);
        var btmp = Number(b[sortColList.titles[sortCol]]);
        if (sortCol >=8 && sortCol <= 9) {
            if (atmp == 0)
                atmp = 99999;
            if (btmp == 0)
                btmp = 99999;
        }
    } else {
        var atmp = String(a[sortColList.titles[sortCol]]).toUpperCase();
        var btmp = String(b[sortColList.titles[sortCol]]).toUpperCase();
    }
    if (atmp < btmp)
        return sortColList.directions[sortCol];
    return -sortColList.directions[sortCol];
}

// проверяем, просрочен ли результат в кэше
function expired(h) {
    return (Date.now()/hour).toFixed(0) - h > expirehours;
}

// Определение цвета по рейтингу
function CalcWrColor(winrate) {
    if (winrate < 46.37) return statcolors.very_bad;
    if (winrate < 49.11) return statcolors.bad;
    if (winrate < 52.45) return statcolors.normal;
    if (winrate < 57.81) return statcolors.good;
    if (winrate < 63.82) return statcolors.very_good;
                        return statcolors.unique;
}

// Определение цвета по рейтингу
function CalcXVMColor(rate) {
    if (isNaN(+rate) && rate != 'XX') return statcolors.undefined;
    if (rate < 16.5) return statcolors.very_bad;
    if (rate < 33.5) return statcolors.bad;
    if (rate < 52.5) return statcolors.normal;
    if (rate < 75.5) return statcolors.good;
    if (rate < 92.5) return statcolors.very_good;
                     return statcolors.unique;
}

// Перевод рейтингов в шкалу XVM
// http://www.koreanrandom.com/forum/topic/2625-/#entry32168
var CalcXrate = {
    wn8: function(rate) {
        if (rate > 3800)
            rate = (100).toFixed(0);
        else
            rate = Math.max(Math.min(rate*(rate*(rate*(rate*(rate*(-0.00000000000000000009762*rate +0.0000000000000016221) -0.00000000001007) +0.000000027916) -0.000036982) +0.05577) -1.3, 100), 0).toFixed(0);
        return FormatX(rate);
    },
    eff: function(rate) {
        if (rate > 2300)
            rate = (100).toFixed(0);
        else
            rate = Math.max(Math.min(rate*(rate*(rate*(rate*(rate*(0.000000000000000006449*rate -0.00000000000004089) +0.00000000008302) -0.00000004433) -0.0000482) +0.1257) -40.42, 100), 0).toFixed(0);
        return FormatX(rate);
    },
    wgr: function(rate) {
        if (rate > 11200)
            rate = (100).toFixed(0);
        else
            rate = Math.max(Math.min(rate*(rate*(rate*(rate*(rate*(-0.000000000000000000001185*rate +0.00000000000000004468) -0.000000000000681) +0.000000005378) -0.00002302) +0.05905) -48.93, 100), 0).toFixed(0);
        return FormatX(rate);
    }
};

// Форматируем значение по шкале XVM в нужный формат
function FormatX(xrate) {
    xrate = String(+xrate);
    if (xrate == 100) return 'XX';
    if (isNaN(xrate)) return '';
    return xrate;
}

// Проверяем, что находимся на нужной страничке
function isInclude(url) {
    for (var m = 0; m < includes.length; m++)
        if ( ~url.indexOf(includes[m]) )
            return true;
    return false;
}
